# fedora spec file for php-icewind-smb3
#
# Copyright (c) 2020 Christopher Engelhard
# License: MIT
#
# Please preserve the changelog entries

# package and composer name
%global vendor      icewind
%global project     smb3

# PHP namespace and directory
%global ns_project  Icewind\\SMB
%global ns_dir      Icewind/SMB3

# Github
%global gh_vendor   icewind1991
%global gh_project  SMB
%global commit      9dba42ab2a3990de29e18cc62b0a8270aceb74e3
%global scommit     %(c=%{commit}; echo ${c:0:7})

# tests
%global with_tests 0

#-- PREAMBLE ------------------------------------------------------------------#
Name:           php-%{vendor}-%{project}
Version:        3.4.1
Release:        %autorelease
Summary:        Php wrapper for smbclient and libsmbclient-php

License:        MIT
URL:            https://github.com/%{gh_vendor}/%{gh_project}
Source0:        https://github.com/%{gh_vendor}/%{gh_project}/archive/%{commit}/%{gh_project}-%{version}-%{scommit}.tar.gz

BuildArch:      noarch

# for the autoloader
Requires:       php-composer(fedora/autoloader)

# from composer.json
Requires:       php(language) >= 7.2
Requires:       php-composer(icewind/streams) >= 0.7.3

# from phpcompatinfo
Requires:       php-date
Requires:       php-json
Requires:       php-mbstring
Requires:       php-pcre
Requires:       php-spl

# for autoloader check
BuildRequires:  php-composer(fedora/autoloader)
BuildRequires:  %{_bindir}/php
BuildRequires:  php(language) >= 7.2
BuildRequires:  php-composer(icewind/streams) >= 0.7.3

%if 0%{?with_tests}
# for tests
BuildRequires:  phpunit8
BuildRequires:  php-date
BuildRequires:  php-json
BuildRequires:  php-mbstring
BuildRequires:  php-pcre
BuildRequires:  php-spl
%endif

# composer provides
Provides:       php-composer(%{vendor}/%{project}) = %{version}

%description
PHP wrapper for smbclient and libsmbclient-php.

Autoloader: %{_datadir}/php/%{ns_dir}/autoload.php


#-- PREP, BUILD & INSTALL -----------------------------------------------------#
%prep
%autosetup -p1 -n %{gh_project}-%{commit}

%build
: Nothing to build.

%install
: Create a PSR-0 tree
mkdir -p   %{buildroot}%{_datadir}/php/%{ns_dir}
cp -pr src/* %{buildroot}%{_datadir}/php/%{ns_dir}

: Generate an autoloader
cat <<'EOF' | tee %{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php
<?php
require_once '%{_datadir}/php/Fedora/Autoloader/autoload.php';

// classes
\Fedora\Autoloader\Autoload::addPsr4('%{ns_project}', __DIR__);

// files & dependencies
\Fedora\Autoloader\Dependencies::required(array(
  '/usr/share/php/Icewind/Streams/autoload.php',
));

\Fedora\Autoloader\Dependencies::optional(array(
   // no optional dependencies
));
EOF

%check
: Check the autoloader
%{_bindir}/php -r '
    require_once "%{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php";
    exit(
        class_exists("\\%{ns_project}\\ServerFactory")
        ? 0 : 1
    );
'
%if 0%{?with_tests}
: Run tests
mkdir vendor
cat << 'EOF' | tee vendor/autoload.php
<?php
require_once '%{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php';
\Fedora\Autoloader\Autoload::addPsr4('%{ns_project}\\Test\\', dirname(__DIR__) . '/tests');
EOF
%{_bindir}/phpunit8 --bootstrap \
    vendor/autoload.php tests
%endif

#-- FILES ---------------------------------------------------------------------#
%files
%license LICENSE
%doc composer.json
%doc README.md
%{_datadir}/php/%{ns_vendor}


#-- CHANGELOG -----------------------------------------------------------------#
%changelog
%autochangelog
