# fedora spec file for php-icewind-searchdav
#
# Copyright (c) 2020 Christopher Engelhard
# License: MIT
#
# Please preserve the changelog entries

# package and composer name
%global vendor      icewind
%global project     searchdav

# PHP namespace and directory
%global ns_project  SearchDAV
%global ns_dir      %( echo "%{ns_project}" | sed  's|\\\\|\/|g' )

# Github
%global gh_vendor   icewind1991
%global gh_project  SearchDAV
%global commit      c69806d900c2c9a5954bfabc80178d6eb0d63df4
%global scommit     %(c=%{commit}; echo ${c:0:7})

# tests
%global with_tests 1

#-- PREAMBLE ------------------------------------------------------------------#
Name:           php-%{vendor}-%{project}
Version:        2.0.0
Release:        %autorelease
Summary:        sabre/dav plugin to implement rfc5323 SEARCH

License:        AGPLv3+
URL:            https://github.com/%{gh_vendor}/%{gh_project}
Source0:        https://github.com/%{gh_vendor}/%{gh_project}/archive/%{commit}/%{gh_project}-%{version}-%{scommit}.tar.gz

BuildArch:      noarch

# for the autoloader
Requires:       php-composer(fedora/autoloader)

# from composer.json
Requires:       php(language) >= 7.1
Requires:       php-composer(sabre/dav) >= 4.0

# from phpcompatinfo
Requires:       php-spl

# for autoloader check
BuildRequires:  php-composer(fedora/autoloader)
BuildRequires:  %{_bindir}/php
BuildRequires:  php(language) >= 7.1
BuildRequires:  php-composer(sabre/dav) >= 4.0

%if 0%{?with_tests}
# for tests
BuildRequires:  phpunit8
BuildRequires:  php-spl
%endif

# composer provides
Provides:       php-composer(%{vendor}/%{project}) = %{version}

%description
A sabre/dav plugin to implement rfc5323 SEARCH.

Autoloader: %{_datadir}/php/%{ns_dir}/autoload.php


#-- PREP, BUILD & INSTALL -----------------------------------------------------#
%prep
%autosetup -p1 -n %{gh_project}-%{commit}

%build
: Nothing to build.

%install
: Create a PSR-0 tree
mkdir -p   %{buildroot}%{_datadir}/php/%{ns_dir}
cp -pr src/* %{buildroot}%{_datadir}/php/%{ns_dir}

: Generate an autoloader
cat <<'EOF' | tee %{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php
<?php
require_once '%{_datadir}/php/Fedora/Autoloader/autoload.php';

// classes
\Fedora\Autoloader\Autoload::addPsr4('%{ns_project}', __DIR__);

// files & dependencies
\Fedora\Autoloader\Dependencies::required(array(
  '/usr/share/php/Sabre/DAV4/autoload.php',
));

\Fedora\Autoloader\Dependencies::optional(array(
   // no optional dependencies
));
EOF

%check
: Check the autoloader
%{_bindir}/php -r '
    require_once "%{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php";
    exit(
        class_exists("\\%{ns_project}\\DAV\\SearchPlugin")
        ? 0 : 1
    );
'
%if 0%{?with_tests}
: Run tests
%{_bindir}/phpunit8 --bootstrap \
    %{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php tests

: Run tests
mkdir vendor
cat << 'EOF' | tee vendor/autoload.php
<?php
require_once '%{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php';
\Fedora\Autoloader\Autoload::addPsr4('%{ns_project}\\Test', dirname(__DIR__) . '/tests');
EOF
%{_bindir}/phpunit8 --bootstrap \
    vendor/autoload.php tests
%endif

#-- FILES ---------------------------------------------------------------------#
%files
%license LICENSE
%doc composer.json
%doc README.md
%{_datadir}/php/%{ns_dir}


#-- CHANGELOG -----------------------------------------------------------------#
%changelog
%autochangelog
