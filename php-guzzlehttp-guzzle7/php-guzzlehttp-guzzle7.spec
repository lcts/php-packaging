# fedora spec file for php-guzzlehttp-guzzle7
#
# Copyright (c) 2020 Christopher Engelhard
# License: MIT
#
# Please preserve the changelog entries

# package and composer name
%global vendor   guzzlehttp
%global project  guzzle7

# PHP namespace and directory
%global ns_project  GuzzleHttp
%global ns_dir      GuzzleHttp7

# Github
%global gh_vendor   guzzle
%global gh_project  guzzle
%global commit      7008573787b430c1c1f650e3722d9bba59967628
%global scommit     %(c=%{commit}; echo ${c:0:7})

# tests
%global with_tests 1

#-- PREAMBLE ------------------------------------------------------------------#
Name:           php-%{vendor}-%{project}
Version:        7.3.0
Release:        %autorelease
Summary:        PHP HTTP client library

License:        MIT
URL:            https://github.com/%{gh_vendor}/%{gh_project}
Source0:        https://github.com/%{gh_vendor}/%{gh_project}/archive/%{commit}/%{gh_project}-%{version}-%{scommit}.tar.gz

BuildArch:      noarch

# for the autoloader
Requires:       php-composer(fedora/autoloader)

# from composer.json
Requires:       php(language) >= 7.2.5
Requires:       php-json
Requires:       php-composer(guzzlehttp/promises) >= 1.4
Requires:       php-composer(guzzlehttp/psr7) >= 1.7
Requires:       php-composer(psr/http-client) >= 1.0
Recommends:     php-curl
Recommends:     php-intl
Recommends:     php-composer(psr/log)

# from phpcompatinfo
Requires:       php-date
Requires:       php-filter
Requires:       php-json
Requires:       php-pcre
Requires:       php-spl

# for autoloader check
BuildRequires:  php-composer(fedora/autoloader)
BuildRequires:  %{_bindir}/php
BuildRequires:  php(language) >= 7.2.5
BuildRequires:  php-composer(guzzlehttp/promises) >= 1.4
BuildRequires:  php-composer(guzzlehttp/psr7) >= 1.7
BuildRequires:  php-composer(psr/http-client) >= 1.0

%if 0%{?with_tests}
# for tests
%endif

# composer provides
Provides:       php-composer(%{vendor}/%{project}) = %{version}
Provides:       php-composer(psr/http-client-implementation) = 1.0

%description
Guzzle 7 HTTP Adapter

Autoloader: %{_datadir}/php/%{ns_dir}/autoload.php


#-- PREP, BUILD & INSTALL -----------------------------------------------------#
%prep
%autosetup -p1 -n %{gh_project}-%{commit}

%build
: Nothing to build.

%install
: Create a PSR-0 tree
mkdir -p   %{buildroot}%{_datadir}/php/%{ns_dir}
cp -pr src/* %{buildroot}%{_datadir}/php/%{ns_dir}

: Generate an autoloader
cat <<'EOF' | tee %{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php
<?php
require_once '%{_datadir}/php/Fedora/Autoloader/autoload.php';

// classes
\Fedora\Autoloader\Autoload::addPsr4('%{ns_project}', __DIR__);

// files & dependencies
\Fedora\Autoloader\Dependencies::required(array(
    __DIR__.'/functions_include.php',
    '/usr/share/php/GuzzleHttp/Promise/autoload.php',
    '/usr/share/php/GuzzleHttp/Psr7/autoload.php',
    '/usr/share/php/Psr/Http/Client/autoload.php',
));

\Fedora\Autoloader\Dependencies::optional(array(
   '/usr/share/php/Psr/log/autoload.php',
));
EOF

%check
: Check the autoloader
%{_bindir}/php -r '
    require_once "%{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php";
    exit(
        class_exists("\\%{ns_project}\\Client") &&
        function_exists("\\%{ns_project}\\describe_type")
        ? 0 : 1
    );
'
%if 0%{?with_tests}
: Run tests
: No tests implemented
%endif

#-- FILES ---------------------------------------------------------------------#
%files
%license LICENSE
%doc composer.json
%doc README.md
%{_datadir}/php/%{ns_dir}


#-- CHANGELOG -----------------------------------------------------------------#
%changelog
%autochangelog
