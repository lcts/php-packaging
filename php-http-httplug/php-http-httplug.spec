# fedora spec file for php-http-httplug
#
# Copyright (c) 2020 Christopher Engelhard
# License: MIT
#
# Please preserve the changelog entries

# package and composer name
%global vendor   php-http
%global project  httplug

# PHP namespace and directory
%global ns_project  Http\\Client
%global ns_dir      %( echo "%{ns_project}" | sed  's|\\\\|\/|g' )

# Github
%global gh_vendor   php-http
%global gh_project  httplug
%global commit      191a0a1b41ed026b717421931f8d3bd2514ffbf9
%global scommit     %(c=%{commit}; echo ${c:0:7})

# tests
%global with_tests 1

#-- PREAMBLE ------------------------------------------------------------------#
Name:           php-http-%{project}
Version:        2.2.0
Release:        %autorelease
Summary:        HTTPlug, the HTTP client abstraction for PHP

License:        MIT
URL:            https://github.com/%{gh_vendor}/%{gh_project}
Source0:        https://github.com/%{gh_vendor}/%{gh_project}/archive/%{commit}/%{gh_project}-%{version}-%{scommit}.tar.gz

BuildArch:      noarch

# for the autoloader
Requires:       php-composer(fedora/autoloader)

# from composer.json
Requires:       php(language) >= 7.1
Requires:       php-composer(php-http/promise) >= 1.1
Requires:       php-composer(psr/http-client) >= 1.0
Requires:       php-composer(psr/http-message) >= 1.0

# from phpcompatinfo
Requires:       php-spl

# for autoloader check
BuildRequires:  php-composer(fedora/autoloader)
BuildRequires:  %{_bindir}/php
BuildRequires:  php(language) >= 7.1
BuildRequires:  php-composer(php-http/promise) >= 1.1
BuildRequires:  php-composer(psr/http-client) >= 1.0
BuildRequires:  php-composer(psr/http-message) >= 1.0

%if 0%{?with_tests}
# for tests
%endif

# composer provides
Provides:       php-composer(%{vendor}/%{project}) = %{version}

%description
HTTP client standard built on PSR-7 HTTP messages. The HTTPlug client interface
is compatible with the official standard for the HTTP client interface, PSR-18.
HTTPlug adds an interface for asynchronous HTTP requests, which PSR-18 does not
cover.

Autoloader: %{_datadir}/php/%{ns_dir}/autoload.php


#-- PREP, BUILD & INSTALL -----------------------------------------------------#
%prep
%autosetup -p1 -n %{gh_project}-%{commit}

%build
: Nothing to build.

%install
: Create a PSR-0 tree
mkdir -p   %{buildroot}%{_datadir}/php/%{ns_dir}
cp -pr src/* %{buildroot}%{_datadir}/php/%{ns_dir}

: Generate an autoloader
cat <<'EOF' | tee %{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php
<?php
require_once '%{_datadir}/php/Fedora/Autoloader/autoload.php';

// classes
\Fedora\Autoloader\Autoload::addPsr4('%{ns_project}', __DIR__);

// files & dependencies
\Fedora\Autoloader\Dependencies::required(array(
   '%{_datadir}/php/Http/Promise/autoload.php',
   '%{_datadir}/php/Psr/Http/Client/autoload.php',
   '%{_datadir}/php/Psr/Http/Message/autoload.php',
));

\Fedora\Autoloader\Dependencies::optional(array(
   // no optional dependencies
));
EOF

%check
: Check the autoloader
%{_bindir}/php -r '
    require_once "%{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php";
    exit(
        class_exists("\\%{ns_project}\\HttpRejectedPromise")
        ? 0 : 1
    );
'
%if 0%{?with_tests}
: Run tests
: No tests implemented
%endif

#-- FILES ---------------------------------------------------------------------#
%files
%license LICENSE
%doc composer.json
%doc README.md
%{_datadir}/php/%{ns_dir}


#-- CHANGELOG -----------------------------------------------------------------#
%changelog
%autochangelog
