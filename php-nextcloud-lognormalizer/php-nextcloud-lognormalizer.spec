# fedora spec file for php-nextcloud-lognormalizer
#
# Copyright (c) 2020 Christopher Engelhard
# License: MIT
#
# Please preserve the changelog entries

# package and composer name
%global vendor   nextcloud
%global project  lognormalizer

# PHP namespace and directory
%global ns_project  Nextcloud\\LogNormalizer
%global ns_dir      %( echo "%{ns_project}" | sed  's|\\\\|\/|g' )

# Github
%global gh_vendor   %{vendor}
%global gh_project  %{project}
%global commit      87445d69225c247aaff64643b1fc83c6d6df741f
%global scommit     %(c=%{commit}; echo ${c:0:7})

# tests
%global with_tests 1

#-- PREAMBLE ------------------------------------------------------------------#
Name:           php-%{vendor}-%{project}
Version:        1.0.0
Release:        %autorelease
Summary:        Parses variables and converts them to string so that they can be logged

License:        AGPLv3
URL:            https://github.com/%{gh_vendor}/%{gh_project}
Source0:        https://github.com/%{gh_vendor}/%{gh_project}/archive/%{commit}/%{gh_project}-%{version}-%{scommit}.tar.gz

BuildArch:      noarch

# for the autoloader
Requires:       php-composer(fedora/autoloader)

# from composer.json
Requires:       php(language) >= 7.2.0
Requires:       php-json

# from phpcompatinfo
Requires:       php-date
Requires:       php-spl

# for autoloader check
BuildRequires:  php-composer(fedora/autoloader)
BuildRequires:  %{_bindir}/php
BuildRequires:  php(language) >= 7.2.0

%if 0%{?with_tests}
# for tests
BuildRequires:  phpunit8
BuildRequires:  php-json
BuildRequires:  php-date
BuildRequires:  php-spl
%endif

# composer provides
Provides:       php-composer(%{vendor}/%{project}) = %{version}

%description
Parses variables and converts them to string so that they can be logged.
Based on the Monolog formatter/normalizer.

Autoloader: %{_datadir}/php/%{ns_dir}/autoload.php


#-- PREP, BUILD & INSTALL -----------------------------------------------------#
%prep
%autosetup -p1 -n %{gh_project}-%{commit}

%build
: Nothing to build.

%install
: Create a PSR-0 tree
mkdir -p   %{buildroot}%{_datadir}/php/%{ns_dir}
cp -pr src/* %{buildroot}%{_datadir}/php/%{ns_dir}

: Generate an autoloader
cat <<'EOF' | tee %{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php
<?php
require_once '%{_datadir}/php/Fedora/Autoloader/autoload.php';

// classes
\Fedora\Autoloader\Autoload::addPsr4('%{ns_project}', __DIR__);

// files & dependencies
\Fedora\Autoloader\Dependencies::required(array(
   // no required dependencies
));

\Fedora\Autoloader\Dependencies::optional(array(
   // no optional dependencies
));
EOF

%check
: Check the autoloader
%{_bindir}/php -r '
    require_once "%{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php";
    exit(
        class_exists("\\%{ns_project}\\Normalizer")
        ? 0 : 1
    );
'
%if 0%{?with_tests}
: Run tests
%{_bindir}/phpunit8 --bootstrap \
    %{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php tests
%endif

#-- FILES ---------------------------------------------------------------------#
%files
%license COPYING
%doc composer.json
%doc README.md
%{_datadir}/php/%{ns_dir}


#-- CHANGELOG -----------------------------------------------------------------#
%changelog
%autochangelog
