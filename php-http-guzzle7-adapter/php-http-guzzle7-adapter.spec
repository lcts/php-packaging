# fedora spec file for php-http-guzzle7-adapter
#
# Copyright (c) 2020 Christopher Engelhard
# License: MIT
#
# Please preserve the changelog entries

# package and composer name
%global vendor   php-http
%global project  guzzle7-adapter

# PHP namespace and directory
%global ns_project  Http\\Adapter\\Guzzle7
%global ns_dir      %( echo "%{ns_project}" | sed  's|\\\\|\/|g' )

# Github
%global gh_vendor   php-http
%global gh_project  guzzle7-adapter
%global commit      fb075a71dbfa4847cf0c2938c4e5a9c478ef8b01
%global scommit     %(c=%{commit}; echo ${c:0:7})

# tests
%global with_tests 1

#-- PREAMBLE ------------------------------------------------------------------#
Name:           php-http-%{project}
Version:        1.0.0
Release:        %autorelease
Summary:        Guzzle 7 HTTP Adapter

License:        MIT
URL:            https://github.com/%{gh_vendor}/%{gh_project}
Source0:        https://github.com/%{gh_vendor}/%{gh_project}/archive/%{commit}/%{gh_project}-%{version}-%{scommit}.tar.gz

BuildArch:      noarch

# for the autoloader
Requires:       php-composer(fedora/autoloader)

# from composer.json
Requires:       php(language) >= 7.2
Requires:       php-composer(php-http/httplug) >= 2.0
Requires:       php-composer(psr/http-client) >= 1.0
Requires:       (php-composer(guzzlehttp/guzzle) >= 7.0 with php-composer(guzzlehttp/guzzle) < 8.0)

# from phpcompatinfo
Requires:       php-spl

# for autoloader check
BuildRequires:  php-composer(fedora/autoloader)
BuildRequires:  %{_bindir}/php
BuildRequires:  php(language) >= 7.2
BuildRequires:  php-composer(php-http/httplug) >= 2.0
BuildRequires:  php-composer(psr/http-client) >= 1.0
BuildRequires:  (php-composer(guzzlehttp/guzzle) >= 7.0 with php-composer(guzzlehttp/guzzle) < 8.0)

%if 0%{?with_tests}
# for tests
%endif

# composer provides
Provides:       php-composer(%{vendor}/%{project}) = %{version}
Provides:       php-composer(php-http/async-client-implementation) = 1.0
Provides:       php-composer(php-http/client-implementation) = 1.0
Provides:       php-composer(psr/http-client-implementation) = 1.0

%description
Guzzle 7 HTTP Adapter

Autoloader: %{_datadir}/php/%{ns_dir}/autoload.php


#-- PREP, BUILD & INSTALL -----------------------------------------------------#
%prep
%autosetup -p1 -n %{gh_project}-%{commit}

%build
: Nothing to build.

%install
: Create a PSR-0 tree
mkdir -p   %{buildroot}%{_datadir}/php/%{ns_dir}
cp -pr src/* %{buildroot}%{_datadir}/php/%{ns_dir}

: Generate an autoloader
cat <<'EOF' | tee %{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php
<?php
require_once '%{_datadir}/php/Fedora/Autoloader/autoload.php';

// classes
\Fedora\Autoloader\Autoload::addPsr4('%{ns_project}', __DIR__);

// files & dependencies
\Fedora\Autoloader\Dependencies::required(array(
   '/usr/share/php/GuzzleHttp7/autoload.php',
   '/usr/share/php/Http/Client/autoload.php',
   '/usr/share/php/Psr/Http/Client/autoload.php',
));

\Fedora\Autoloader\Dependencies::optional(array(
   // no optional dependencies
));
EOF

%check
: Check the autoloader
%{_bindir}/php -r '
    require_once "%{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php";
    exit(
        class_exists("\\%{ns_project}\\Client")
        ? 0 : 1
    );
'
%if 0%{?with_tests}
: Run tests
: No tests implemented
%endif

#-- FILES ---------------------------------------------------------------------#
%files
%license LICENSE
%doc composer.json
%doc README.md
%{_datadir}/php/%{ns_dir}


#-- CHANGELOG -----------------------------------------------------------------#
%changelog
%autochangelog
