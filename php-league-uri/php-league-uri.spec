# fedora spec file for php-league-uri
#
# Copyright (c) 2020 Christopher Engelhard
# License: MIT
#
# Please preserve the changelog entries

# package and composer name
%global vendor      league
%global project     uri

# PHP namespace and directory
%global ns_project  League\\Uri
%global ns_dir      %( echo "%{ns_project}" | sed  's|\\\\|\/|g' )

# Github
%global gh_vendor   thephpleague
%global gh_project  uri
%global commit      09da64118eaf4c5d52f9923a1e6a5be1da52fd9a
%global scommit     %(c=%{commit}; echo ${c:0:7})

# tests
%global with_tests 1

#-- PREAMBLE ------------------------------------------------------------------#
Name:           php-%{vendor}-%{project}
Version:        6.4.0
Release:        %autorelease
Summary:        URI manipulation library

License:        MIT
URL:            https://github.com/%{gh_vendor}/%{gh_project}
Source0:        https://github.com/%{gh_vendor}/%{gh_project}/archive/%{commit}/%{gh_project}-%{version}-%{scommit}.tar.gz

BuildArch:      noarch

# for the autoloader
Requires:       php-composer(fedora/autoloader)

# from composer.json
Requires:       php(language) >= 7.2
Requires:       php-json
Requires:       php-composer(league/uri-interface) >= 2.1
Requires:       php-composer(psr/http-message) >= 1.0
Recommends:     php-intl
Recommends:     php-composer(psr/http-factory)
#Suggests:     php-composer(league/uri-components) ## not packaged

# from phpcompatinfo
Requires:       php-fileinfo
Requires:       php-filter
Requires:       php-pcre
Requires:       php-spl

# for autoloader check
BuildRequires:  php-composer(fedora/autoloader)
BuildRequires:  %{_bindir}/php
BuildRequires:  php(language) >= 7.0
BuildRequires:  php-composer(league/uri-interface) >= 2.1
BuildRequires:  php-composer(psr/http-message) >= 1.0

%if 0%{?with_tests}
# for tests
%endif

# composer provides
Provides:       php-composer(%{vendor}/%{project}) = %{version}
Conflicts:      php-composer(%{vendor}/uri-schemes) >= 1.0

%description
The Uri package provides simple and intuitive classes to manage URIs in PHP.
You will be able to parse, build and resolve URIs, create URIs from different
sources (string, PHP environment, base URI, URI template, ...), handle
internalisation and infer properties and features from URIs.

Autoloader: %{_datadir}/php/%{ns_dir}/autoload.php


#-- PREP, BUILD & INSTALL -----------------------------------------------------#
%prep
%autosetup -p1 -n %{gh_project}-%{commit}

%build
: Nothing to build.

%install
: Create a PSR-0 tree
mkdir -p   %{buildroot}%{_datadir}/php/%{ns_dir}
cp -pr src/* %{buildroot}%{_datadir}/php/%{ns_dir}

: Generate an autoloader
cat <<'EOF' | tee %{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php
<?php
require_once '%{_datadir}/php/Fedora/Autoloader/autoload.php';

// classes
\Fedora\Autoloader\Autoload::addPsr4('%{ns_project}', __DIR__);

// files & dependencies
\Fedora\Autoloader\Dependencies::required(array(
  '/usr/share/php/League/UriInterfaces/autoload.php',
  '/usr/share/php/Psr/Http/Message/autoload.php',
));

\Fedora\Autoloader\Dependencies::optional(array(
   '/usr/share/php/Psr/Http/Factory',
));
EOF

%check
: Check the autoloader
%{_bindir}/php -r '
    require_once "%{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php";
    exit(
        class_exists("\\%{ns_project}\\UriTemplate") 
        ? 0 : 1
    );
'
%if 0%{?with_tests}
: Run tests
: No tests implemented
%endif

#-- FILES ---------------------------------------------------------------------#
%files
%license LICENSE
%doc composer.json
%{_datadir}/php/%{ns_dir}


#-- CHANGELOG -----------------------------------------------------------------#
%changelog
%autochangelog
