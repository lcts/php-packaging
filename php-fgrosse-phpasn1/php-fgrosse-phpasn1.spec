# fedora spec file for php-fgrosse-phpasn1
#
# Copyright (c) 2020 Christopher Engelhard
# License: MIT
#
# Please preserve the changelog entries

# package and composer name
%global vendor      fgrosse
%global project     phpasn1

# PHP namespace and directory
%global ns_project  FG
%global ns_dir      %( echo "%{ns_project}" | sed  's|\\\\|\/|g' )

# Github
%global gh_vendor   FGrosse
%global gh_project  PHPASN1
%global commit      20299033c35f4300eb656e7e8e88cf52d1d6694e
%global scommit     %(c=%{commit}; echo ${c:0:7})

# tests
%global with_tests 1

#-- PREAMBLE ------------------------------------------------------------------#
Name:           php-%{vendor}-%{project}
Version:        2.3.0
Release:        %autorelease
Summary:        Thin assertion library for input validation in business models

License:        MIT
URL:            https://github.com/%{gh_vendor}/%{gh_project}
Source0:        https://github.com/%{gh_vendor}/%{gh_project}/archive/%{commit}/%{gh_project}-%{version}-%{scommit}.tar.gz

BuildArch:      noarch

# for the autoloader
Requires:       php-composer(fedora/autoloader)

# from composer.json
Requires:       php(language) >= 7.0
Recommends:     php-gmp
Recommends:     php-bcmath
Recommends:     php-curl

# from phpcompatinfo
Requires:       php-date
Requires:       php-pcre
Requires:       php-spl

# for autoloader check
BuildRequires:  php-composer(fedora/autoloader)
BuildRequires:  %{_bindir}/php
BuildRequires:  php(language) >= 7.0

%if 0%{?with_tests}
# for tests
%endif

# composer provides
Provides:       php-composer(%{vendor}/%{project}) = %{version}

%description
A PHP Framework that allows you to encode and decode arbitrary ASN.1 structures
using the ITU-T X.690 Encoding Rules. This encoding is very frequently used in
X.509 PKI environments or the communication between heterogeneous computer
systems.

The API allows you to encode ASN.1 structures to create binary data such as
certificate signing requests (CSR), X.509 certificates or certificate
revocation lists (CRL). PHPASN1 can also read BER encoded binary data into
separate PHP objects that can be manipulated by the user and reencoded
afterwards.

Autoloader: %{_datadir}/php/%{ns_dir}/autoload.php


#-- PREP, BUILD & INSTALL -----------------------------------------------------#
%prep
%autosetup -p1 -n %{gh_project}-%{commit}

%build
: Nothing to build.

%install
: Create a PSR-0 tree
mkdir -p   %{buildroot}%{_datadir}/php/%{ns_dir}
cp -pr lib/* %{buildroot}%{_datadir}/php/%{ns_dir}

: Generate an autoloader
cat <<'EOF' | tee %{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php
<?php
require_once '%{_datadir}/php/Fedora/Autoloader/autoload.php';

// classes
\Fedora\Autoloader\Autoload::addPsr4('%{ns_project}', __DIR__);

// files & dependencies
\Fedora\Autoloader\Dependencies::required(array(
  // no mandatory dependencies
));

\Fedora\Autoloader\Dependencies::optional(array(
   // no optional dependencies
));
EOF

%check
: Check the autoloader
%{_bindir}/php -r '
    require "%{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php";
    exit(
        class_exists("\\%{ns_project}\\ASN1\\Universal\\Integer")
        ? 0 : 1
    );
'
%if 0%{?with_tests}
: Run tests
: No tests implemented
%endif

#-- FILES ---------------------------------------------------------------------#
%files
%license LICENSE
%doc composer.json
%doc README.md
%{_datadir}/php/%{ns_dir}


#-- CHANGELOG -----------------------------------------------------------------#
%changelog
%autochangelog
