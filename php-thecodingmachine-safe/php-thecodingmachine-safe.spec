# fedora spec file for php-thecodingmachine-safe
#
# Copyright (c) 2020 Christopher Engelhard
# License: MIT
#
# Please preserve the changelog entries

# package and composer name
%global vendor      thecodingmachine
%global project     safe

# PHP namespace and directory
%global ns_project  Safe
%global ns_dir      %( echo "%{ns_project}" | sed  's|\\\\|\/|g' )

# Github
%global gh_vendor   thecodingmachine
%global gh_project  safe
%global commit      a8ab0876305a4cdaef31b2350fcb9811b5608dbc
%global scommit     %(c=%{commit}; echo ${c:0:7})

# tests
%global with_tests 1

#-- PREAMBLE ------------------------------------------------------------------#
Name:           php-%{vendor}-%{project}
Version:        1.3.3
Release:        %autorelease
Summary:        PHP core functions that throw exceptions instead of returning FALSE on error

License:        MIT
URL:            https://github.com/%{gh_vendor}/%{gh_project}
Source0:        https://github.com/%{gh_vendor}/%{gh_project}/archive/%{commit}/%{gh_project}-%{version}-%{scommit}.tar.gz
Source1:        autoload.php

BuildArch:      noarch

# for the autoloader
Requires:       php-composer(fedora/autoloader)

# from composer.json
Requires:       php(language) >= 7.2

# from phpcompatinfo
# don't add requires, as any package requiring a function from
# this library will also require the corresponding extension

# for autoloader check
BuildRequires:  php-composer(fedora/autoloader)
BuildRequires:  %{_bindir}/php
BuildRequires:  php(language) >= 7.2

%if 0%{?with_tests}
# for tests
%endif

# composer provides
Provides:       php-composer(%{vendor}/%{project}) = %{version}

%description
A set of core PHP functions rewritten to throw exceptions instead of returning
false when an error is encountered.

This project is deprecated.

Autoloader: %{_datadir}/php/%{ns_dir}/autoload.php


#-- PREP, BUILD & INSTALL -----------------------------------------------------#
%prep
%autosetup -p1 -n %{gh_project}-%{commit}

%build
: Nothing to build.

%install
: Create a PSR-0 tree
mkdir -p   %{buildroot}%{_datadir}/php/%{ns_dir}
cp -pr lib %{buildroot}%{_datadir}/php/%{ns_dir}
cp -pr generated %{buildroot}%{_datadir}/php/%{ns_dir}
cp -pr deprecated %{buildroot}%{_datadir}/php/%{ns_dir}

: Add autoloader
cp %{SOURCE1} %{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php

%check
: Check the autoloader
%{_bindir}/php -r '
    require_once "%{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php";
    exit(
        class_exists("\\%{ns_project}\\DateTime") &&
        function_exists("\\%{ns_project}\\json_encode")
        ? 0 : 1
    );
'
%if 0%{?with_tests}
: Run tests
: No tests implemented
%endif

#-- FILES ---------------------------------------------------------------------#
%files
%license LICENSE
%doc composer.json
%doc README.md
%{_datadir}/php/%{ns_dir}


#-- CHANGELOG -----------------------------------------------------------------#
%changelog
%autochangelog
