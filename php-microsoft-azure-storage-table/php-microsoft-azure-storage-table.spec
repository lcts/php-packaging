# fedora spec file for php-microsoft-azure-storage-table
#
# Copyright (c) 2020 Christopher Engelhard
# License: MIT
#
# Please preserve the changelog entries

# package and composer name
%global vendor   microsoft
%global project  azure-storage-table

# PHP namespace and directory
%global ns_project  MicrosoftAzure\\Storage\\Table
%global ns_dir      %( echo "%{ns_project}" | sed  's|\\\\|\/|g' )

# Github
%global gh_vendor   Azure
%global gh_project  azure-storage-table-php
%global commit      747b7c47adf4d9aa42999f7d3c1ef1b0cbfbc25d
%global scommit     %(c=%{commit}; echo ${c:0:7})

# tests
%global with_tests 1

#-- PREAMBLE ------------------------------------------------------------------#
Name:           php-%{vendor}-%{project}
Version:        1.1.4
Release:        %autorelease
Summary:        A set of PHP client libraries that make it easy to access Microsoft Azure Storage Table APIs

License:        MIT
URL:            https://github.com/%{gh_vendor}/%{gh_project}
Source0:        https://github.com/%{gh_vendor}/%{gh_project}/archive/%{commit}/%{gh_project}-%{version}-%{scommit}.tar.gz

BuildArch:      noarch

# for the autoloader
Requires:       php-composer(fedora/autoloader)

# from composer.json
Requires:       php(language) >= 5.6.0
Requires:       php-composer(microsoft/azure-storage-common) >= 1.5

# from phpcompatinfo
Requires:       php-date
Requires:       php-hash
Requires:       php-json
Requires:       php-pcre
Requires:       php-spl

# for autoloader check
BuildRequires:  php-composer(fedora/autoloader)
BuildRequires:  %{_bindir}/php
BuildRequires:  php(language) >= 5.6.0
BuildRequires:  php-composer(microsoft/azure-storage-common) >= 1.5

%if 0%{?with_tests}
# for tests
%endif

# composer provides
Provides:       php-composer(%{vendor}/%{project}) = %{version}

%description
A PHP client library that makes it easy to access Microsoft Azure Storage Table
services.

Autoloader: %{_datadir}/php/%{ns_dir}/autoload.php


#-- PREP, BUILD & INSTALL -----------------------------------------------------#
%prep
%setup -q -n %{gh_project}-%{commit}

%build
: Nothing to build.

%install
: Create a PSR-0 tree
mkdir -p   %{buildroot}%{_datadir}/php/%{ns_dir}
cp -pr src/Table/* %{buildroot}%{_datadir}/php/%{ns_dir}

: Generate an autoloader
cat <<'EOF' | tee %{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php
<?php
require_once '%{_datadir}/php/Fedora/Autoloader/autoload.php';

// classes
\Fedora\Autoloader\Autoload::addPsr4('%{ns_project}', __DIR__);

// files & dependencies
\Fedora\Autoloader\Dependencies::required(array(
    '%{_datadir}/php/MicrosoftAzure/Storage/Common/autoload.php',
));

\Fedora\Autoloader\Dependencies::optional(array(
   // no optional dependencies
));
EOF

%check
: Check the autoloader
%{_bindir}/php -r '
    require_once "%{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php";
    exit(
        class_exists("\\%{ns_project}\\TableRestProxy")
        ? 0 : 1
    );
'
%if 0%{?with_tests}
: Run tests
: No tests implemented
%endif

#-- FILES ---------------------------------------------------------------------#
%files
%license LICENSE
%doc composer.json
%doc README.md
%{_datadir}/php/%{ns_dir}


#-- CHANGELOG -----------------------------------------------------------------#
%changelog
%autochangelog
