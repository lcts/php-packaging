# fedora spec file for php-web-auth-webauthn-lib
#
# Copyright (c) 2020 Christopher Engelhard
# License: MIT
#
# Please preserve the changelog entries

# package and composer name
%global vendor      web-auth
%global project     webauthn-lib

# PHP namespace and directory
%global ns_project  Webauthn
%global ns_dir      Webauthn/Lib

# Github
%global gh_vendor   web-auth
%global gh_project  webauthn-lib
%global commit      04b98ee3d39cb79dad68a7c15c297c085bf66bfe
%global scommit     %(c=%{commit}; echo ${c:0:7})

# tests
%global with_tests 1

#-- PREAMBLE ------------------------------------------------------------------#
Name:           php-%{vendor}-%{project}
Version:        3.3.9
Release:        %autorelease
Summary:        FIDO2/Webauthn Support For PHP

License:        MIT
URL:            https://github.com/%{gh_vendor}/%{gh_project}
Source0:        https://github.com/%{gh_vendor}/%{gh_project}/archive/%{commit}/%{gh_project}-%{version}-%{scommit}.tar.gz

BuildArch:      noarch

# for the autoloader
Requires:       php-composer(fedora/autoloader)

# from composer.json
Requires:       php(language) >= 7.2
Requires:       php-json
Requires:       php-openssl
Requires:       php-mbstring
Requires:       php-composer(beberlei/assert) >= 3.2
Requires:       php-composer(fgrosse/phpasn1) >= 2.1
Requires:       php-composer(psr/http-client) >= 1.0
Requires:       php-composer(psr/http-factory) >= 1.0
Requires:       php-composer(psr/http-message) >= 1.0
Requires:       php-composer(psr/log) >= 1.1
Requires:       php-composer(ramsey/uuid) >= 3.8
Requires:       php-composer(spomky-labs/base64url) >= 2.0
Requires:       php-composer(spomky-labs/cbor-php) >= 1.1
Requires:       php-composer(symfony/process) >= 3.0
Requires:       php-composer(thecodingmachine/safe) >= 1.1
Requires:       php-composer(web-auth/cose-libs) = %{version}
Requires:       php-composer(web-auth/metadata-service) = %{version}
Recommends:     php-composer(psr/log-implementation)
#Recommends:     php-composer(web-token/jwt-key-mgmt) ## not packaged
#Recommends:     php-composer(web-token/jwt-signature-algorithm-rsa) ## not packaged
#Recommends:     php-composer(web-token/jwt-signature-algorithm-ecdsa) ## not packaged
#Recommends:     php-composer(web-token/jwt-signature-algorithm-eddsa) ## not packaged

# from phpcompatinfo
Requires:  php-date
Requires:  php-hash
Requires:  php-spl

# for autoloader check
BuildRequires:  php-composer(fedora/autoloader)
BuildRequires:  %{_bindir}/php
BuildRequires:  php(language) >= 7.2
BuildRequires:  php-composer(beberlei/assert) >= 3.2
BuildRequires:  php-composer(fgrosse/phpasn1) >= 2.1
BuildRequires:  php-composer(psr/http-client) >= 1.0
BuildRequires:  php-composer(psr/http-factory) >= 1.0
BuildRequires:  php-composer(psr/http-message) >= 1.0
BuildRequires:  php-composer(psr/log) >= 1.1
BuildRequires:  php-composer(ramsey/uuid) >= 3.8
BuildRequires:  php-composer(spomky-labs/base64url) >= 2.0
BuildRequires:  php-composer(spomky-labs/cbor-php) >= 1.1
BuildRequires:  php-composer(symfony/process) >= 3.0
BuildRequires:  php-composer(thecodingmachine/safe) >= 1.1
BuildRequires:  php-composer(web-auth/cose-libs) = %{version}
BuildRequires:  php-composer(web-auth/metadata-service) = %{version}

%if 0%{?with_tests}
# for tests
%endif

# composer provides
Provides:       php-composer(%{vendor}/%{project}) = %{version}

%description
FIDO2/Webauthn Support for PHP is a PHP library that will help you to support
compatible security tokens and devices.

Autoloader: %{_datadir}/php/%{ns_dir}/autoload.php


#-- PREP, BUILD & INSTALL -----------------------------------------------------#
%prep
%autosetup -p1 -n %{gh_project}-%{commit}

%build
: Nothing to build.

%install
: Create a PSR-0 tree
mkdir -p   %{buildroot}%{_datadir}/php/%{ns_dir}
cp -pr src/* %{buildroot}%{_datadir}/php/%{ns_dir}

: Generate an autoloader
cat <<'EOF' | tee %{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php
<?php
require_once '%{_datadir}/php/Fedora/Autoloader/autoload.php';

// classes
\Fedora\Autoloader\Autoload::addPsr4('%{ns_project}', __DIR__);

\Fedora\Autoloader\Dependencies::required(array(
  '/usr/share/php/Beberlei/Assert/autoload.php',
  '/usr/share/php/FGrosse/PHPASN1/autoload.php',
  '/usr/share/php/Psr/Http/Client/autoload.php',
  '/usr/share/php/Psr/Http/Factory/autoload.php',
  '/usr/share/php/Psr/Http/Message/autoload.php',
  '/usr/share/php/Psr/Log/autoload.php',
  '/usr/share/php/Ramsey/Uuid/autoload.php',
  '/usr/share/php/Base64Url/autoload.php',
  '/usr/share/php/CBOR/autoload.php',
  '/usr/share/php/Symfony/Process/autoload.php',
  '/usr/share/php/Safe/autoload.php',
  '/usr/share/php/Cose/autoload.php',
  '/usr/share/php/Webauthn/MetadataService/autoload.php',
));

\Fedora\Autoloader\Dependencies::optional(array(
   '/usr/share/php/Psr/Log/Implementation',
));
EOF

%check
: Check the autoloader
%{_bindir}/php -r '
    require_once "%{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php";
    exit(
        class_exists("\\%{ns_project}\\Credential")
        ? 0 : 1
    );
'
%if 0%{?with_tests}
: Run tests
: No tests implemented
%endif

#-- FILES ---------------------------------------------------------------------#
%files
%license LICENSE
%doc composer.json
%{_datadir}/php/%{ns_dir}


#-- CHANGELOG -----------------------------------------------------------------#
%changelog
%autochangelog
