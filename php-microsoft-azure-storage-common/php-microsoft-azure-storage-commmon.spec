# fedora spec file for php-microsoft-azure-storage-common
#
# Copyright (c) 2020 Christopher Engelhard
# License: MIT
#
# Please preserve the changelog entries

# package and composer name
%global vendor   microsoft
%global project  azure-storage-common

# PHP namespace and directory
%global ns_project  MicrosoftAzure\\Storage\\Common
%global ns_dir      %( echo "%{ns_project}" | sed  's|\\\\|\/|g' )

# Github
%global gh_vendor   Azure
%global gh_project  azure-storage-common-php
%global commit      e5738035891546075bd369954e8af121d65ebd6d
%global scommit     %(c=%{commit}; echo ${c:0:7})

# tests
%global with_tests 1

#-- PREAMBLE ------------------------------------------------------------------#
Name:           php-%{vendor}-%{project}
Version:        1.5.1
Release:        %autorelease
Summary:        A set of common code shared by Azure Storage Blob, Table, Queue and File PHP client libraries

License:        MIT
URL:            https://github.com/%{gh_vendor}/%{gh_project}
Source0:        https://github.com/%{gh_vendor}/%{gh_project}/archive/%{commit}/%{gh_project}-%{version}-%{scommit}.tar.gz

BuildArch:      noarch

# for the autoloader
Requires:       php-composer(fedora/autoloader)

# from composer.json
Requires:       php(language) >= 5.6.0
Requires:       (php-composer(guzzlehttp/guzzle) >= 6.0 with php-composer(guzzlehttp/guzzle) < 8.0)

# from phpcompatinfo
Requires:       php-simplexml
Requires:       php-bcmath
Requires:       php-ctype
Requires:       php-curl
Requires:       php-date
Requires:       php-filter
Requires:       php-hash
Requires:       php-json
Requires:       php-libxml
Requires:       php-openssl
Requires:       php-pcre
Requires:       php-xmlwriter

# for autoloader check
BuildRequires:  php-composer(fedora/autoloader)
BuildRequires:  %{_bindir}/php
BuildRequires:  php(language) >= 5.6.0
BuildRequires:  (php-composer(guzzlehttp/guzzle) >= 6.0 with php-composer(guzzlehttp/guzzle) < 8.0)

%if 0%{?with_tests}
# for tests
%endif

# composer provides
Provides:       php-composer(%{vendor}/%{project}) = %{version}

%description
This project provides a set of common PHP code shared by Azure Storage Blob,
Table, Queue and File PHP client libraries.

Autoloader: %{_datadir}/php/%{ns_dir}/autoload.php


#-- PREP, BUILD & INSTALL -----------------------------------------------------#
%prep
%autosetup -p1 -n %{gh_project}-%{commit}

%build
: Nothing to build.

%install
: Create a PSR-0 tree
mkdir -p   %{buildroot}%{_datadir}/php/%{ns_dir}
cp -pr src/Common/* %{buildroot}%{_datadir}/php/%{ns_dir}

: Generate an autoloader
cat <<'EOF' | tee %{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php
<?php
require_once '%{_datadir}/php/Fedora/Autoloader/autoload.php';

// classes
\Fedora\Autoloader\Autoload::addPsr4('%{ns_project}', __DIR__);

// files & dependencies
\Fedora\Autoloader\Dependencies::required(array(
    array(
        '%{_datadir}/php/GuzzleHttp6/autoload.php',
        '%{_datadir}/php/GuzzleHttp7/autoload.php',
    ),
));

\Fedora\Autoloader\Dependencies::optional(array(
   // no optional dependencies
));
EOF

%check
: Check the autoloader
%{_bindir}/php -r '
    require_once "%{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php";
    exit(
        class_exists("\\%{ns_project}\\Logger")
        ? 0 : 1
    );
'
%if 0%{?with_tests}
: Run tests
: No tests implemented
%endif

#-- FILES ---------------------------------------------------------------------#
%files
%license LICENSE
%doc composer.json
%doc README.md
%{_datadir}/php/%{ns_dir}


#-- CHANGELOG -----------------------------------------------------------------#
%changelog
%autochangelog
