# fedora spec file for php-web-auth-metadata-service
#
# Copyright (c) 2020 Christopher Engelhard
# License: MIT
#
# Please preserve the changelog entries

# package and composer name
%global vendor      web-auth
%global project     metadata-service

# PHP namespace and directory
%global ns_project  Webauthn\\MetadataService
%global ns_dir      %( echo "%{ns_project}" | sed  's|\\\\|\/|g' )

# Github
%global gh_vendor   web-auth
%global gh_project  webauthn-metadata-service
%global commit      8488d3a832a38cc81c670fce05de1e515c6e64b1
%global scommit     %(c=%{commit}; echo ${c:0:7})

# tests
%global with_tests 1

#-- PREAMBLE ------------------------------------------------------------------#
Name:           php-%{vendor}-%{project}
Version:        3.3.9
Release:        %autorelease
Summary:        Metadata Service for FIDO2/Webauthn

License:        MIT
URL:            https://github.com/%{gh_vendor}/%{gh_project}
Source0:        https://github.com/%{gh_vendor}/%{gh_project}/archive/%{commit}/%{gh_project}-%{version}-%{scommit}.tar.gz

BuildArch:      noarch

# for the autoloader
Requires:       php-composer(fedora/autoloader)

# from composer.json
Requires:       php(language) >= 7.2
Requires:       php-json
Requires:       php-composer(beberlei/assert) >= 3.2
Requires:       php-composer(league/uri) >= 6.0
Requires:       php-composer(psr/http-client) >= 1.0
Requires:       php-composer(psr/http-factory) >= 1.0
Requires:       php-composer(psr/log) >= 1.1
#Recommends:     php-composer(web-token/jwt-key-mgmt) ## not packaged
#Recommends:     php-composer(web-token/jwt-signature-algorithm-ecdsa) ## not packaged

# from phpcompatinfo
Requires:       php-hash
Requires:       php-spl

# for autoloader check
BuildRequires:  php-composer(fedora/autoloader)
BuildRequires:  %{_bindir}/php
BuildRequires:  php(language) >= 7.2
BuildRequires:  php-composer(beberlei/assert) >= 3.2
BuildRequires:  php-composer(league/uri) >= 6.0
BuildRequires:  php-composer(psr/http-client) >= 1.0
BuildRequires:  php-composer(psr/http-factory) >= 1.0
BuildRequires:  php-composer(psr/log) >= 1.1

%if 0%{?with_tests}
# for tests
%endif

# composer provides
Provides:       php-composer(%{vendor}/%{project}) = %{version}

%description
A library that adds several tools to contact the FIDO Alliance or any other
compliant Metadata Services.

Autoloader: %{_datadir}/php/%{ns_dir}/autoload.php


#-- PREP, BUILD & INSTALL -----------------------------------------------------#
%prep
%autosetup -p1 -n %{gh_project}-%{commit}

%build
: Nothing to build.

%install
: Create a PSR-0 tree
mkdir -p   %{buildroot}%{_datadir}/php/%{ns_dir}
cp -pr src/* %{buildroot}%{_datadir}/php/%{ns_dir}

: Generate an autoloader
cat <<'EOF' | tee %{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php
<?php
require_once '%{_datadir}/php/Fedora/Autoloader/autoload.php';

// classes
\Fedora\Autoloader\Autoload::addPsr4('%{ns_project}', __DIR__);

\Fedora\Autoloader\Dependencies::required(array(
  '/usr/share/php/Beberlei/Assert/autoload.php',
  '/usr/share/php/League/Uri/autoload.php',
  '/usr/share/php/Psr/Http/Client/autoload.php',
  '/usr/share/php/Psr/Http/Factory/autoload.php',
  '/usr/share/php/Psr/Log/autoload.php',
));

\Fedora\Autoloader\Dependencies::optional(array(
   // no optional dependencies
));
EOF

%check
: Check the autoloader
%{_bindir}/php -r '
    require_once "%{buildroot}%{_datadir}/php/%{ns_dir}/autoload.php";
    exit(
        class_exists("\\%{ns_project}\\Utils")
        ? 0 : 1
    );
'
%if 0%{?with_tests}
: Run tests
: No tests implemented
%endif

#-- FILES ---------------------------------------------------------------------#
%files
%license LICENSE
%doc composer.json
%{_datadir}/php/%{ns_dir}


#-- CHANGELOG -----------------------------------------------------------------#
%changelog
%autochangelog
